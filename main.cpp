#include <iostream>
#include <ctime>
#include <string.h>

using namespace std;

struct Film{
    char Movie[50];
    char Director[50];
    char Genre[50];
    float Rating;
    float Price;
};

void Inscription(Film pat)
{
    cout << "Title: " << pat.Movie;
    cout << "\nProd: " << pat.Director;
    cout << "\nGenre: " << pat.Genre;
    cout << "\nRating: " << pat.Rating;
    cout << "\nPrice: " << pat.Price;
    cout << "\n\n";
}

//Поиск по названию
bool NameSearch(Film film[], char  user[], int lenght)
{
    bool flag = 0;
    for (int i = 0; i < lenght; i++) {
        if (strstr(film[i].Movie, user) != NULL)
        {
            cout << film[i].Movie << " ";
            flag = true;
            break;
        }
        else {flag = false;}
    }
    return flag;
}

// Поиск по жанру
void GenreSearch(Film film, char user[])
{
    if (strstr(film.Genre, user) != NULL)
    {
        cout << film.Movie << " ";
    }
}

//Поиск по режисеру
void ProdSearch(Film film, char user[])
{
    if (strstr(film.Director, user) != NULL)
    {
        cout << film.Movie << " ";
    }
}

int main()
{
    cout << "\t\tKinoPoisk\n";
    const int lenght = 9;
    int input = 0;

    Film kinopoisk[lenght]
            {
                    {"1) Avengers", "K.Feige", "fantasy", 9.5, 120},
                    {"2) Fast and furious", "V.Diesel", "action", 9.2, 99.5},
                    {"3) End of time", "C.Hellsgord", "drama", 9.2, 70},
                    {"4) Angry Birds", "T.Van Orman", "cartoons", 8.5, 110},
                    {"5) No Man's Land", "M.Lyons", "action", 7.5, 74.3},
                    {"6) Scooby-Doo", "E.Spaulding", "cartoons", 9.2, 87.8},
                    {"7) Arctic Apocalypse", "J.Condelic", "fantasy", 6.3, 96},
                    {"8) The Death of **** Long", "D.Scheinert", "drama", 6.7, 100},
                    {"9) Three X", "V.Diesel", "action", 8.8, 93.4},
            };
    do {
        cout << "\t     Selection menu\n";
        cout << "Search by title - [1]\n";
        cout << "Search by genre - [2]\n";
        cout << "Search by director - [3]\n";
        cout << "Show all posts - [4]\n";
        cout << "Input: ";
        cin >> input;
        cout << "\n";

        switch (input) {
            case 1:
                char  user1[50];
                cout << "Enter movie title: ";
                cin >> user1;

                if (NameSearch(kinopoisk, user1, lenght) == true)
                {
                    cout << "(film found)\n\n";
                }
                else{
                    cout << "(film not found)\n\n";
                }
                break;

            case 2:
                char user2[50];
                cout << "Enter the movie genre: ";
                cin >> user2;
                for (int i = 0; i < lenght; i++) {
                    GenreSearch(kinopoisk[i], user2);
                }
                cout << "\n";
                break;

            case 3:
                char user3[50];
                cout << "Enter movie producer: ";
                cin >> user3;
                for (int i = 0; i < lenght; i++) {
                    ProdSearch(kinopoisk[i], user3);
                }
                cout << "\n";
                break;

            case 4:
                for (int i = 0; i < lenght; ++i) {
                    Inscription(kinopoisk[i]);
                }
                break;

            case 5:
            default:
                break;
        }
    } while (input = 4);

    cout << "\n\n";
system("pause");
    return 0;
}
